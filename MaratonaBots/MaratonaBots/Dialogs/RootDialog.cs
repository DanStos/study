﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace MaratonaBots.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;

            

            await context.PostAsync("**Olá, tudo bem?**");

            var message = activity.CreateReply();

            if(activity.Text.Equals("herocard",StringComparison.InvariantCultureIgnoreCase))
            {
                var heroCard = new HeroCard();
                heroCard.Title = "Planeta";
                heroCard.Subtitle = "Universo";
                heroCard.Images = new List<CardImage>
                 {
                    new CardImage("https://cdnbr1.img.sputniknews.com/images/673/05/6730548.jpg","Planeta")
                 }; message.Attachments.Add(heroCard.ToAttachment());
            }
            else if (activity.Text.Equals("videocard", StringComparison.InvariantCultureIgnoreCase))
            {

                var videoCard = new VideoCard();
                videoCard.Title = "o FAMOSO";
                //videoCard.Subtitle = "Insert your subtitle";
                videoCard.Autostart = true;
                videoCard.Autoloop = false;
                videoCard.Media = new List<MediaUrl>
                {
                    new MediaUrl("https://www.youtube.com/watch?v=qjjT960FYt4")

                }; message.Attachments.Add(videoCard.ToAttachment());

            }
            else if (activity.Text.Equals("audiocard", StringComparison.InvariantCultureIgnoreCase))
            {

                var audio = CreateAudioCard();
                message.Attachments.Add(audio);

            }
            else if (activity.Text.Equals("animation", StringComparison.InvariantCultureIgnoreCase))
            {

                var attachment = CreateAnimationCard();
                message.Attachments.Add(attachment);
                
            }
            else if(activity.Text.Equals("carousel",StringComparison.InvariantCultureIgnoreCase))
            {
                message.AttachmentLayout = AttachmentLayoutTypes.Carousel;

                var audio = CreateAudioCard();
                var animation = CreateAnimationCard();

                message.Attachments.Add(audio);
                message.Attachments.Add(animation);
            }
                        
            await context.PostAsync(message);

            context.Wait(MessageReceivedAsync);
        }

        private Attachment CreateAnimationCard()
        {
            var animationCard = new AnimationCard();
            animationCard.Title = "Gif fofo";
            animationCard.Subtitle = "lindo demais";
            animationCard.Autostart = true;
            animationCard.Autoloop = false;
            animationCard.Media = new List<MediaUrl>
                {
                    new MediaUrl("https://img.buzzfeed.com/buzzfeed-static/static/enhanced/webdr06/2013/5/31/10/anigif_enhanced-buzz-3734-1370010471-16.gif")

                }; return animationCard.ToAttachment();
        }

        private Attachment CreateAudioCard()
        {
            var audioCard = new AudioCard();
            audioCard.Title = "One ring to rule them all";
            audioCard.Image = new ThumbnailUrl("https://vignette.wikia.nocookie.net/lotr/images/6/65/One_Ring_Render.png/revision/latest?cb=20150218005107", "Thumbnail");
            audioCard.Subtitle = "There u go";
            audioCard.Autostart = true;
            audioCard.Autoloop = false;
            audioCard.Media = new List<MediaUrl>
                {
                    new MediaUrl("http://www.wavlist.com/movies/338/lotr-onering.wav")

                }; return audioCard.ToAttachment();
        }
    }
}