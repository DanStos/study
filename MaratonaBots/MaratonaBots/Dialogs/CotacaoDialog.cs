﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;

namespace MaratonaBots.Dialogs
{

    [Serializable]

    [LuisModel("82a57e2e-11a1-479c-a483-d8fad7690c3e", "0ed4ad62c9e64220b5a18054dc3b59a1")]
    public class CotacaoDialog : LuisDialog<object>
    {
        [LuisIntent("None")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Desculpa, não consegui entender o que você quis dizer. " +
                "Poderia repetir por gentileza?");
        }

        [LuisIntent("Cumprimento")]
        public async Task Cumprimento(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("E ai mano, firme? Só sei falar cotação de moeda por enquanto...");
        }

        [LuisIntent("Cotacao")]
        public async Task Cotacao(IDialogContext context, LuisResult result)
        {
            var moedas = result.Entities?.Select(e => e.Entity);

            await context.PostAsync($"Já te trago o valor do {string.Join(",", moedas.ToArray())}");
        }
    }
}