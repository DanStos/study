﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Teste.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        
        
        [HttpGet("{Name}")]

        public IEnumerable<object> Get(string Name)
        {
             
             //Create a query
             HttpClient client = new HttpClient();
            client.BaseAddress = new Uri($"https://api.tibiadata.com/v2/characters/{Name}.json");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync($"https://api.tibiadata.com/v2/characters/{Name}.json").Result;


            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                var s = JsonConvert.DeserializeObject(result);
                yield return s;
            }
            else
            {
                yield return "fail";
            }

        } 

    }

    
}
